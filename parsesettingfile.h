#ifndef PARSESETTINGFILE_H
#define PARSESETTINGFILE_H

#include <QObject>
#include <QFile>
#include <QTextStream>
#include <QDir>
#include <QDebug>
#include <QTextCodec>
#include "security.h"


//配置文件中配置参数格式的头定义
#define     SEPARATOR           " #--# "            //分隔符
#define     FONT_TYPE           "<FontType="        //字体类型
#define     FONT_SIZE           "<FontSize="        //字体大小
#define     CODE_TYPE           "<CodeType="        //编码类型
#define     ADB_PATH            "<ADBPath="         //ADB路径
#define     REGISTER_NUM        "<RegisterNumber="  //软件注册码
#define     HISTORY_PATH        "<HistoryPath="     //文件打开历史记录
#define     IS_APPLY_OFFLINE_MODE       "<IsApplyOfflineMode="      //是否应用与离线模式
#define     IS_APPLY_REALTIME_MODE      "<IsApplyRealtimeMode="     //是否应用于实时模式
#define     IS_FIRST_SYS_FILTER         "<IsFirstSysFilter="        //是否启用一级系统过滤
#define     IS_SECOND_SYS_FILTER        "<IsSecondSysFilter="       //是否启用二级系统过滤
#define     FIRST_SYS_FILTERS           "<FirstSysFilters="         //一级系统过滤表
#define     SECOND_SYS_FILTERS          "<SecondSysFilters="        //二级系统过滤表
#define     CUR_DEBUG_APP       "<CurDebugApp="     //当前调试的APP包名
#define     IS_APPLY_DEBUG_APP  "<IsApplyDebugApp=" //是否启用调试指定APP功能



class ParseSettingFile : public QObject
{
    Q_OBJECT
public:
    explicit ParseSettingFile(QObject *parent = 0,QString path = "");
    
signals:
    
public slots:
    
public:
    bool checkFile(QString path);           //检查文件是否为设置文件
    QString getFontType();                  //获取字体类型
    int getFontSize();                      //获取字体大小
    QString getCodeType();                  //获取文本编码类型
    QString getADBPath();                   //获取ADB路径
    QString getRegisterNum();               //获取软件注册码
    QStringList getHistory();               //获取文件打开的历史记录
    bool getIsApplyOfflineMode();           //获取是否应用于离线模式
    bool getIsApplyRealtimeMode();          //获取是否应用于实时模式
    bool getIsFirstFilter();                //获取是否启用一级过滤
    bool getIsSecondFilter();               //获取是否启用二级过滤
    QStringList getFirstSysFilters();       //获取一级系统过滤表
    QStringList getSecondSysFilters();      //获取二级系统过滤表
    bool getIsApplyDebugApp();              //获取是否启用调试指定APP功能
    QString getCurDebugApp();               //获取当前调试程序的包名
    bool saveParamsToFile(QString path,QStringList paramList);  //保存参数到文件中
    bool saveHistoryToFile(QString path,QStringList history);   //保存文件打开历史记录到文件中
    bool saveRegisterNumToFile(QString path,QString num);       //保存软件注册码包文件中
    QString createParamsItem(QString key,QString value);        //创建一个参数项（其格式为：键$@$值）
    
private:
    void initEnvironment();                 //初始化解析器运行环境
    void loadDefaultConfig();               //加载默认的配置
    QStringList createCurParams();          //创建当前的配置参数表
    void parseFile(QString path);           //解析配置文件
    bool stringIsNum(QString str);          //判断一个字符串是否可以转换为数字
    QString getKey(QString params);         //从一个参数项中获取键
    QString getValue(QString params);       //从一个参数项中获取值
    QString getValueByKey(QStringList paramList,QString key); //从参数集合中查找指定键对应的值
    bool isContainKey(QStringList paramList,QString key);     //判断某个参数集合中是否包含一个键
    bool isDirExist(QString fullPath,bool isCreate);          //判断一个路径是否存在，若不存在，可选择是否创建该路径
    //重新构建指定键值在配置文件中的字符串，如"<FontType=Courier New>"
    QString updateNewValue(QStringList paramList,QString key,QStringList &isSaveList);
    
private:
    QStringList mDefaultParams;     //默认参数表
    int mParamsNum;                 //当前系统支持的参数数目
    QStringList mHistoryList;       //文件打开历史记录
    
    QString fontType;               //字体类型
    int fontSize;                   //字体大小
    QString codeType;               //编码类型
    QString adbPath;                //ADB路径
    QString registerNum;            //软件注册码
    bool isApplyOfflineMode;        //是否应用于离线模式
    bool isApplyRealtimeMode;       //是否应用于实时模式
    bool isFirstFilter;             //是否启用一级过滤
    bool isSecondFilter;            //是否启用二级过滤
    QString firstSysFilters;        //一级系统过滤表
    QString secondSysFilters;       //二级系统过滤表
    QString curDebugApp;            //当前调试的APP包名
    bool isApplyDebugApp;           //是否启用调试指定APP功能
};

#endif // PARSESETTINGFILE_H
